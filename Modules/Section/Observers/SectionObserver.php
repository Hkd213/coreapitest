<?php
namespace Modules\Section\Observers;

use Illuminate\Support\Facades\Auth;
use Modules\Section\Entities\Section;

class SectionObserver
{

    /**
     * Model Saving Course from Eloquent
     *
     * @param $model
     */
    public function saving(Section $model)
    {
        $model->author_id = Auth::id();
    }

}