<?php
namespace Modules\Section\Admin;

use Modules\Lesson\Entities\Lesson;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Facades\TableColumn;

class SectionLessons extends Section
{

    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'Section Lessons';
    }

    public function initialize()
    {
        $this->creating(function ($config, Model $model) {
            $model->author_id = Auth::id();
            $model->type = 1;
        });
    }


    public function onDisplay($scopes = null)
    {
        $display = AdminDisplay::tree(OrderTreeType::class);

        if ($scopes) {
            $display->setScopes($scopes);
        }

        $display->setOrderField('order')->setValue(function ($model) {
            //In here can render your own template))
            return TableColumn::text('lesson.title')->setModel($model);
        });

        return $display;
    }

    public function onEdit($id)
    {
        return $form = AdminForm::panel()
            ->addBody(
                AdminFormElement::select('lessons_id', 'Lesson')
                    ->setModelForOptions(new Lesson())
                    ->setDisplay('title')
                    ->required(),
                AdminFormElement::number('order', 'Order'),
                AdminFormElement::hidden('section_id')
            );
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return boolean
     */
    public function onDelete($id)
    {
        return true;
    }
}
