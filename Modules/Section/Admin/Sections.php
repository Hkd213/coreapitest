<?php

namespace Modules\Section\Admin;

use Illuminate\Database\Eloquent\Model;
use Modules\Lesson\Entities\Lesson;
use Modules\Section\Entities\SectionLesson;
use SleepingOwl\Admin\Facades\Admin;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Facades\TableColumn;

class Sections extends Section
{

    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->creating(function ($config, Model $model) {
            $model->author_id = \Auth::id();
        });
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'Section';
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getEditTitle()
    {
        return 'Edit section';
    }


    public function onDisplay($scopes = null)
    {
        $display = AdminDisplay::datatablesAsync();

        if ($scopes) {
            $display->setScopes($scopes);
        }

        $display->setColumns([
            TableColumn::text('title', 'Title'),
            TableColumn::text('description', 'Description'),
            TableColumn::lists('lessons.title')
                ->setLabel('Lessons'),
//            TableColumn::relatedLink('sales_person.fullname')
//                ->setLabel(trans('realtors::back/subscription.column.user_id_sp')),
            TableColumn::datetime('updated_at', 'Updated At'),
            TableColumn::datetime('create_at', 'Created At')
        ]);
        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminDisplay::tabbed();
        $display->setTabs(function () use ($id) {
            $tabs = [];

            $form = AdminForm::panel();

            $form->addBody(
                AdminFormElement::text('title', 'Title')
                    ->required(),
                AdminFormElement::hidden('course_id'),
                AdminFormElement::text('description', 'Description'),
                AdminFormElement::multiselectajax('lessons', 'lessons')
                    ->setModelForOptions(new Lesson())
                    ->setDisplay('title')
                    ->required(),
                AdminFormElement::hidden('author_id')
            );

            $tabs[] = AdminDisplay::tab($form)->setLabel('Main')
                ->setActive(true)
                ->setIcon('<i class="fa fa-file-text"></i>');

            $lesson = Admin::getModel(SectionLesson::class)
                ->fireDisplay(['scopes' => ['withSection', $id]]);

            $lesson->setParameter('section_id', $id);

            $tabs[] = AdminDisplay::tab($lesson)
                ->setLabel('Lessons')
                ->setIcon('<i class="fa fa-file-code-o"></i>');

            return $tabs;
        });

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return boolean
     */
    public function onDelete($id)
    {
        return true;
    }
}
