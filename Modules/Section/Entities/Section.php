<?php

namespace Modules\Section\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Lesson\Entities\Lesson;
use Modules\Section\Observers\SectionObserver;

class Section extends Model
{

    protected $table    = 'sections';
    protected $fillable = ['title', 'author_id', 'description'];

    /**
     * Load Observer
     */
    public static function boot()
    {
        self::observe(new SectionObserver());
        parent::boot();
    }

    public function scopeWithCourse($query, $course_id)
    {
        $query->where('course_id', $course_id);
    }

    public function lessons()
    {
        return $this->belongsToMany(
            Lesson::class,
            'sections_lessons',
            'section_id',
            'lesson_id'
        );
    }

}