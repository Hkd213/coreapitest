<?php
namespace Modules\Section\Entities;


use Illuminate\Database\Eloquent\Model;
use Modules\Lesson\Entities\Lesson;

class SectionLesson extends Model
{

    public $table    = 'sections_lessons';
    public $fillable = ['lesson_id', 'section_id', 'order'];

    public function lesson()
    {
        return $this->hasOne(Lesson::class, 'id', 'lesson_id');
    }

    public function section()
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function scopeWithSection($query, $section_id)
    {
        $query->where('section_id', $section_id);
    }

    public function getOrderField()
    {
        return 'order';
    }

}