<?php

Route::group(['middleware' => 'web', 'prefix' => 'section', 'namespace' => 'Modules\Section\Http\Controllers'], function()
{
    Route::get('/', 'SectionController@index');
});
