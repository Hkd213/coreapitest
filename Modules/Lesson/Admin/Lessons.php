<?php

namespace Modules\Lesson\Admin;

use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Facades\TableColumn;

class Lessons extends Section
{

    /**
     * @var string
     */
    protected $title;

    protected $redirect = ['create' => 'display'];

    protected $checkAccess = true;

    private $types = [
        'video',
        'article'
    ];

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
//        $this->creating(function ($config, Model $model) {
//            $model->user_id = Auth::id();
//        });
    }

    public function getTitle()
    {
        return 'Lecture';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('author')
            ->setOrder([0, 'desc']);
        /** @noinspection PhpParamsInspection */
        $display->setColumns([
            TableColumn::text('id')
                ->setLabel('ID'),
            TableColumn::text('title')
                ->setLabel('Title'),
            TableColumn::text('multi_media', 'Preview')
                ->setView(view('admin::partias.field.media_preview')),
            //            TableColumn::text('category')
            //                ->setLabel('Category'),
            TableColumn::text('description', 'Description'),
            TableColumn::checkbox('status')
                ->setLabel('Status'),
            TableColumn::text('author.name', 'Author'),
            TableColumn::datetime('updated_at')
                ->setLabel('Update At'),
            TableColumn::datetime('created_at')
                ->setLabel('Create At'),
        ]);

        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $display = AdminDisplay::tabbed();
        $display->setTabs(function () use ($id) {
            $tabs = [];
            $form = AdminForm::panel();

            $form->addBody(
                AdminFormElement::text('title', 'Title')
                    ->required(),
                AdminFormElement::ckeditor('description', 'Description')
                    ->required(),
                AdminFormElement::select('type', 'Type')
                    ->setOptions($this->types),
                AdminFormElement::file('multi_media', 'Video file')
                    ->addValidationRule('mimetypes:video/avi,video/mpeg,video/quicktime,image/jpeg,image/bmp,image/png')
                    ->required()
            );

            $tabs[] = AdminDisplay::tab($form)->setLabel('Lesson')
                ->setActive(true);

            return $tabs;
        });

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        return true;
    }
}