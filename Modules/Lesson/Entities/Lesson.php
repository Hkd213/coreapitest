<?php

namespace Modules\Lesson\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\Employee;
use Modules\Lesson\Observers\LessonObserver;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Lesson extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
     * Load Observer
     */
    public static function boot()
    {
        self::observe(new LessonObserver());
        parent::boot();
    }

    protected $table    = 'lessons';
    protected $fillable = ['title', 'type', 'status'];

    public function author()
    {
        return $this->hasOne(Employee::class,  'id','author_id');
    }

}