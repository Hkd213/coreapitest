<?php

namespace Modules\Lesson\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Modules\Lesson\Entities\Lesson;

class LessonObserver
{

    /**
     * Model Saved Course from Eloquent
     *
     * @param $model
     */
    public function saved($model)
    {

    }

    /**
     * Model Saving Course from Eloquent
     *
     * @param $model
     */
    public function saving(Lesson $model)
    {
        //array
        $multi_media = Request::file('multi_media');

        if ($multi_media) {
            foreach ($multi_media as $media) {
                $model->addMedia($media)->toMediaCollection();
            }

        } else {
            if ($model->multi_media) {
                $model->addMedia($model->multi_media)->toMediaCollection();
            }
        }
        unset($model->multi_media);
        $model->author_id = Auth::id();
    }
}