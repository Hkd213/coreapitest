<?php

Route::group(['middleware' => 'web', 'prefix' => 'lesson', 'namespace' => 'Modules\Lesson\Http\Controllers'], function()
{
    Route::get('/', 'LessonController@index');
});
