<?php

namespace Modules\Employee\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class EmployeeObserver
{


    /**
     * Model Saving Course from Eloquent
     *
     * @param $model
     */
    public function creating($model)
    {
        if ($model->password) {
            $model->password = bcrypt($model->password);
        }
    }

    public function updating(Model $model)
    {
        if( strlen($model->password) != 60 && !preg_match('/^\$2y\$/', $model->password ))
        {
            $model->password = bcrypt($model->password);
        }
    }
}