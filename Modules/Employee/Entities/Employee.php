<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Modules\Employee\Observers\EmployeeObserver;
use Spatie\Permission\Traits\HasRoles;

class Employee extends Authenticatable
{

    use Notifiable;
    use HasRoles;

    protected $guard_name = 'backend';


    protected $table    = 'employee';
    protected $fillable = ['name', 'email', 'password', 'last_login'];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public static function boot()
    {
        Employee::observe(new EmployeeObserver());
        parent::boot();
    }

    public function getEmployeeResponse()
    {
        return $this;
    }

    public function scopeWithoutMe($query)
    {
        return $query->where('id', '!=', Auth::id());
    }
}
