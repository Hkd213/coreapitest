<?php
namespace Modules\Employee\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Facades\TableColumn;
use Spatie\Permission\Models\Role;

class Employees extends Section
{

    /**
     * @var string
     */
    protected $title;

    protected $redirect = ['create' => 'display'];

    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->creating(function ($config, Model $model) {
            $model->user_id = Auth::id();
        });
    }

    public function getTitle()
    {
        return 'Employees';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('roles')
            ->setOrder([0, 'desc']);
        /** @noinspection PhpParamsInspection */
        $display->setColumns([
            TableColumn::text('id')
                ->setLabel('ID'),
            TableColumn::text('name')
                ->setLabel('Name'),
            TableColumn::text('email')
                ->setLabel('Email'),
            TableColumn::lists('roles.name')
                ->setLabel('Roles'),
            TableColumn::datetime('last_login')
            ->setLabel('Last Login'),
            TableColumn::datetime('updated_at')
                ->setLabel('Update At'),
            TableColumn::datetime('created_at')
                ->setLabel('Create At'),
        ]);

        $display->getScopes()
            ->push('withoutMe');

        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $display = AdminDisplay::tabbed();
        $display->setTabs(function () use ($id) {
            $tabs = [];
            $form = AdminForm::panel();

            $form->addBody(
                AdminFormElement::text('email', 'Email')
                    ->required()
                    ->unique()
                    ->addValidationRule('email'),
                AdminFormElement::password('password', 'Password')
                    ->required()
                    ->addValidationRule('min:6'),
                AdminFormElement::text('name', 'Name')
                    ->required(),
                AdminFormElement::multiselectajax('roles', 'Roles')
                    ->setModelForOptions(new Role())
                    ->setDisplay('name')
                    ->required()
            );

            $tabs[] = AdminDisplay::tab($form)->setLabel('Main')
                ->setActive(true)
                ->setIcon('<i class="fa fa-credit-card"></i>');

            return $tabs;
        });

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        return true;
    }
}