<?php

namespace Modules\Employee\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\Employee;

class EmployeeDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $user = Employee::create([
            'name'     => 'tester',
            'email'    => 'test@test.com',
            'password' => 123123
        ]);

        if($user)
        {
            $user->assignRole('admin');
        }
    }
}
