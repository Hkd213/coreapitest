<?php

namespace Modules\Employee\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Employee\Entities\Employee;
use Modules\User\Http\Requests\EditRequest;
use Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @resource Employees
 *
 * Course
 */
class ApiEmployeeController extends Controller
{

    /**
     * Get Employee
     *
     * @param  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $user = Employee::findOrFail($id);
        $success["employee"] = $user->getEmployeeResponse();

        return response()->apiSuccess($success);
    }

}
