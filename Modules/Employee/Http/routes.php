<?php

Route::group(['middleware' => ['auth:api','api_doc'], 'prefix' => 'v1'], function(){
    Route::get('employee/{id}', 'Modules\Employee\Http\Controllers\ApiEmployeeController@get');
});