<?php

Route::group(['middleware' => 'api_doc', 'prefix' => 'v1'], function () {
    Route::post('user', 'Modules\Auth\Http\Controllers\AuthController@register')->name('register');
    Route::post('user/login', 'Modules\Auth\Http\Controllers\AuthController@login')->name('login');

    Route::post('user/password/email',
        'App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
    Route::post('user/password/reset', 'App\Http\Controllers\Auth\ResetPasswordController@reset');
    Route::post('user/logout',
        'Modules\Auth\Http\Controllers\AuthController@logout')->middleware('auth:api')->name('logout');
});

Route::group(['middleware' => 'web', 'prefix' => 'backend'], function () {
    Route::get('password/reset', 'Modules\Auth\Http\Controllers\Admin\ForgotPasswordController@showLinkRequestForm')
        ->name('backend.password.request');
    Route::post('password/email',
        'Modules\Auth\Http\Controllers\Admin\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'Modules\Auth\Http\Controllers\Admin\ResetPasswordController@reset');
    Route::get('password/reset/{token?}',
        'Modules\Auth\Http\Controllers\Admin\ResetPasswordController@showResetForm')->name('password.reset');
    Route::get('login', 'Modules\Auth\Http\Controllers\Admin\AuthController@showLoginForm')->name('backend.login');
    Route::post('login', 'Modules\Auth\Http\Controllers\Admin\AuthController@login')->name('backend.login.submit');
    Route::post('logout', 'Modules\Auth\Http\Controllers\Admin\AuthController@logout')->name('backend.logout');
});