<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Http\Requests\AuthRequest;
use Modules\User\Models\User;
/**
 * @resource Authorisation
 *
 * Authorisation
 */
class AuthController extends Controller
{

    use AuthenticatesUsers;


    /**
     * Auth user
     * return token. Headers: [ ‘Accept’ => ‘application/json’, ‘Authorization’ => ‘Bearer ‘.$token ]
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $this->validateLogin($request);

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user             = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;

            return response()->apiSuccess($success);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Register user
     *
     * @param AuthRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(AuthRequest $request)
    {
        $input             = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user              = User::create($input);

        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['user']  = $user->getUserResponse();

        return response()->apiSuccess($success);
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::user()->tokens()->delete();
        $success["result"] = 1;

        return response()->apiSuccess($success);
    }

}
