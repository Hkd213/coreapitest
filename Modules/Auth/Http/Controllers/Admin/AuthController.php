<?php

namespace Modules\Auth\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'backend/login';


    public function __construct()
    {
        $this->middleware('guest:backend')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth::admin-login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect(route('backend.login'));
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $user->last_login = date('Y-m-d H:i:s');
        $user->save();
    }

    protected function guard()
    {
        return Auth::guard('backend');
    }

}