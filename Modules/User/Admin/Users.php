<?php

namespace Modules\User\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Facades\TableColumn;
use Spatie\Permission\Models\Role;

class Users extends Section
{

    /**
     * @var string
     */
    protected $title;

    protected $redirect = ['create' => 'display'];

    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {

    }

    public function getTitle()
    {
        return 'Users';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc']);
        /** @noinspection PhpParamsInspection */
        $display->setColumns([
            TableColumn::text('id')
                ->setLabel('ID'),
            TableColumn::text('full_name')
                ->setLabel('Name'),
            TableColumn::text('email')
                ->setLabel('Email'),
            TableColumn::text('address')
                ->setLabel('Address'),
            TableColumn::text('country')
                ->setLabel('Country'),
            TableColumn::datetime('updated_at')
                ->setLabel('Update At'),
            TableColumn::datetime('created_at')
                ->setLabel('Create At'),
        ]);

        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $display = AdminDisplay::tabbed();
        $display->setTabs(function () use ($id) {
            $tabs = [];
            $form = AdminForm::panel();

            $form->addBody(
                AdminFormElement::text('email', 'Email')
                    ->required()
                    ->unique()
                    ->addValidationRule('email'),
                AdminFormElement::text('fname', 'First name')
                    ->required(),
                AdminFormElement::text('lname', 'Last name')
                    ->required(),
                AdminFormElement::text('address', 'Address'),
                AdminFormElement::text('country', 'Country')
            );

            $tabs[] = AdminDisplay::tab($form)->setLabel('Main')
                ->setActive(true)
                ->setIcon('<i class="fa fa-credit-card"></i>');

            return $tabs;
        });

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        return true;
    }
}