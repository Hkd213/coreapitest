<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParameterUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['name'])->default("");
            $table->string('fname')->default("");
            $table->string('lname')->default("");
            $table->string('job_title')->default("");
            $table->string('address')->default("");
            $table->string('zip_code')->default("");
            $table->string('country')->default("");
            $table->string('city')->default("");
            $table->string('tax_number')->default("");
            $table->string('company_name')->default("");
            $table->string('role')->default("");
            $table->integer('subscription')->default(0);
            $table->integer('plan')->default(0);
            $table->integer('active')->default(1);
            $table->string('coupon')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['fname','lname','job_title','address','zip_code','country','city','tax_number','company_name','role','subscription','plan','active','coupon']);
        });
    }
}
