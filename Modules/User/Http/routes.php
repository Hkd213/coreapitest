<?php

Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function () {
//    Route::get('/', 'UserController@index');
});

Route::group(['middleware' => ['auth:api','api_doc'], 'prefix' => 'v1/user'], function(){
    Route::delete('/{id?}', 'Modules\User\Http\Controllers\ApiUserController@delete');
    Route::put('/{id?}', 'Modules\User\Http\Controllers\ApiUserController@edit');
});
