<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\User\Http\Requests\EditRequest;
use Modules\User\Http\Requests\ForgotRequest;
use Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;
/**
 * @resource Users
 *
 * Users
 */
class ApiUserController extends Controller
{
    /**
     * Delete user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        $user        = Auth::user();
        $user->delete();
        $success["result"] = 1;
        return response()->apiSuccess($success);
    }
    /**
     * Edit user
     *
     * @param EditRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(EditRequest $request)
    {
        $user        = Auth::user();
        $input             = $request->all();
        $user->update($input);
        $user->save();
        $success["user"] = $user->getUserResponse();
        return response()->apiSuccess($success);
    }

    /**
     * Get user
     *
     * @param EditRequest $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $user=User::findOrFail($id);
        $success["user"] = $user;
        return response()->apiSuccess($success);
    }


}
