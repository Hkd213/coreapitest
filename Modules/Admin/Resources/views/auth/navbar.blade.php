@if ($user)
    <li class="dropdown dropdown-toggle user user-menu" data-toggle="dropdown" style="margin-right: 20px;">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
            <img src="{{ $user->avatar }}" class="user-image" />
            <span class="hidden-xs">{{ $user->name }}</span>
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
                <img src="{{ $user->avatar }}" class="img-circle" />

                <p>
                    {{ $user->name }}
                    <small>{{trans('sleeping_owl::lang.auth.since', ['date' => $user->created_at->format('d.m.Y')])}}</small>
                </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">

                <a href="{{ route('backend.logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{trans('sleeping_owl::lang.auth.logout')}}
                </a>

                <form id="logout-form" action="{{ route('backend.logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </li>
@endif
