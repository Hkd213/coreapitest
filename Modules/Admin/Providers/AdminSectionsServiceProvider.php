<?php

namespace Modules\Admin\Providers;

use Illuminate\Routing\Router;
use Illuminate\Database\Eloquent\Factory;
use Modules\Admin\Widgets\NavigationUserBlock;
use Modules\Course\Admin\Courses;
use Modules\Course\Entities\Course;
use Modules\Employee\Admin\Employees;
use Modules\Employee\Entities\Employee;
use Modules\Lesson\Admin\Lessons;
use Modules\Lesson\Entities\Lesson;
use Modules\Permission\Admin\Permissions;
use Modules\Permission\Admin\Roles;
use Modules\Section\Admin\SectionLessons;
use Modules\Section\Admin\Sections;
use Modules\Section\Entities\Section;
use Modules\Section\Entities\SectionLesson;
use Modules\Subscription\Admin\Subscriptions;
use Modules\User\Admin\Users;
use Modules\User\Models\User;
use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Contracts\Navigation\NavigationInterface;
use SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Laravel\Cashier\Subscription;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $widgets = [
        NavigationUserBlock::class
    ];


    protected $sections = [
        Employee::class      => Employees::class,
        Permission::class    => Permissions::class,
        Role::class          => Roles::class,
        Subscription::class  => Subscriptions::class,
        User::class          => Users::class,
        Section::class       => Sections::class,
        Lesson::class        => Lessons::class,
        Course::class        => Courses::class,
        SectionLesson::class => SectionLessons::class
    ];

    /**
     * Boot the application events.
     * @param Admin $admin
     *
     * @return void
     */
    public function boot(Admin $admin)
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerPolicies("Modules\\Admin\\Policy\\");

        $this->app->call([$this, 'registerRoutes']);
        $this->app->call([$this, 'registerNavigation']);
        $this->app->call([$this, 'registerViewsAdmin']);

        parent::boot($admin);
    }

    /**
     * @param Router $router
     */
    public function registerRoutes(Router $router)
    {
        $router->group([
            'prefix'     => config('sleeping_owl.url_prefix'),
            'middleware' => config('sleeping_owl.middleware')
        ], function ($router) {
            require base_path('Modules/Admin/Http/routes.php');
        });
    }

    /**
     * @param NavigationInterface $navigation
     */
    public function registerNavigation(NavigationInterface $navigation)
    {
        require base_path('Modules/Admin/Support/navigation.php');
    }

    /**
     * @param null $namespace
     * @return array
     */
    public function policies($namespace = null)
    {
        if (is_null($namespace)) {
            $namespace = config('sleeping_owl.policies_namespace', '\\Modules\\Admin\\Policy');
        }

        $policies = [];

        foreach ($this->sections as $model => $section) {
            $policies[$section] = $namespace . 'AdminPolicy';
        }

        return $policies;
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('admin.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'admin'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/admin');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/admin';
        }, \Config::get('view.paths')), [$sourcePath]), 'admin');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/admin');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'admin');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'admin');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if ( ! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * @param WidgetsRegistryInterface $widgetsRegistry
     */
    public function registerViewsAdmin(WidgetsRegistryInterface $widgetsRegistry)
    {
        foreach ($this->widgets as $widget) {

            $widgetsRegistry->registerWidget($widget);
        }
    }
}
