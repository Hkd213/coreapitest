<?php

use Laravel\Cashier\Subscription;
use Modules\Course\Entities\Course;
use Modules\Employee\Entities\Employee;
use Modules\Lesson\Entities\Lesson;
use Modules\Section\Entities\Section;
use Modules\User\Models\User;
use SleepingOwl\Admin\Navigation\Page;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

$navigation->setFromArray([
    (new Page(Lesson::class))
        ->setIcon('fa fa-file-text'),
    (new Page(Course::class))
        ->setIcon('fa fa-align-justify'),
    (new Page(Employee::class))
        ->setIcon('fa fa-users'),
    (new Page(User::class))
        ->setIcon('fa fa-user-circle'),
    [
        'title' => 'Permissions',
        'icon'  => 'fa fa-hand-stop-o',
        'pages' => [
            (new Page(Permission::class))
                ->setIcon('fa fa-hand-o-left'),
            (new Page(Role::class))
                ->setIcon('fa fa-user-o')
        ]
    ],
    (new Page(Subscription::class))
        ->setIcon('fa fa-handshake-o')
]);