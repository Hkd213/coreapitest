<?php
namespace Modules\Admin\Policy;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;
use Modules\Employee\Entities\Employee;

class AdminPolicy
{
    use HandlesAuthorization;


    public function before(Employee $user, $ability, ...$item)
    {
        if (Auth::check()) {
            if (in_array($ability, ['display', 'create', 'edit', 'delete'])) {
                $model = isset($item[1]) ? $item[1] : $item[0];

                return $this->can($ability, $user, $model);
            }

            return false;
        }

        return false;
    }

    public function can($ability, Employee $user,  $model)
    {
        $class = class_basename($model);

        return $user->hasRole('admin') || $user->givePermissionTo($class.'.'.$ability);
    }

    public function display(Employee $user, $ability, ...$item)
    {
        return false;
    }

    public function create(Employee $user, $ability, ...$item)
    {
        return false;
    }

    public function edit(Employee $user, $ability, ...$item)
    {
        return false;
    }

    public function delete(Employee $user, $ability, ...$item)
    {
        return false;
    }
}