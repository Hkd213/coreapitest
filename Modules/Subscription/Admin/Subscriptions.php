<?php
namespace Modules\Subscription\Admin;

use SleepingOwl\Admin\Section;
use AdminDisplay;
use SleepingOwl\Admin\Facades\TableColumn;

class Subscriptions extends Section
{

    /**
     * @var string
     */
    protected $title;

    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {

    }

    public function getTitle()
    {
        return 'Subscriptions';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('owner')
            ->setOrder([0, 'desc']);
        /** @noinspection PhpParamsInspection */
        $display->setColumns([
            TableColumn::text('id')
                ->setLabel('ID'),
            TableColumn::text('stripe_plan')
                ->setLabel('Plan'),
            TableColumn::text('quantity')
                ->setLabel('Quantity'),
            TableColumn::text('owner')
                ->setView(view('admin::partias.field.user_url'))
                ->setLabel('User'),
            TableColumn::datetime('ends_at')
                ->setLabel('Ends At'),
            TableColumn::datetime('updated_at')
                ->setLabel('Update At'),
            TableColumn::datetime('created_at')
                ->setLabel('Create At'),
        ]);

        $display->paginate(15);

        return $display;
    }
}