<?php

namespace Modules\Subscription\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Subscription\Http\Requests\SubscriptionRequest;
use Modules\User\Models\User;

/**
 * @resource Subscription
 *
 * Subscription
 */
class ApiSubscriptionController extends Controller
{
    /**
     * Create subscription. Example token tok_visa
     *
     * @param SubscriptionRequest $request
     * @return Response
     */
    public function store(SubscriptionRequest $request)
    {
        /** @var $user User $user */
        $user   = Auth::user();
        $token  = $request->stripe_token;
        try {
            $result = $user->newSubscription('coreapi', 'yearly')->create($token);
        } catch (\Exception $e)
        {
            return response()->apiError($e->getMessage());
        }

        return response()->apiSuccess($result);
    }

}
