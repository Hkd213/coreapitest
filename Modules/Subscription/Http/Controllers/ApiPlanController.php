<?php

namespace Modules\Subscription\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Subscription\Http\Transformers\PlanTransformer;
use Stripe\Plan;
use Stripe\Stripe;

/**
 * @resource Plan
 *
 * Plan
 */
class ApiPlanController extends Controller
{

    /**
     * List plans
     *
     */

    public function index()
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        $plans    = Plan::all();
        $resource = fractal($plans->data, new PlanTransformer());

        return response()->apiSuccess($resource);
    }
}