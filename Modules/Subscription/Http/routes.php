<?php

Route::group(['middleware' => ['api_doc'], 'prefix' => 'v1', 'namespace' => 'Modules\Subscription\Http\Controllers'], function () {
    Route::group(['middleware' => 'auth:api'], function ()
    {
            Route::post('subscription', 'ApiSubscriptionController@store');

    });

    Route::get('/plan', 'ApiPlanController@index');
});