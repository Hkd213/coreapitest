<?php
namespace Modules\Subscription\Http\Transformers;

use League\Fractal\TransformerAbstract;

class PlanTransformer extends TransformerAbstract
{
    public function transform($plan)
    {
        return [
            'id'       => (string)$plan->id,
            'name'     => (string)$plan->nickname,
            'active'   => (string)$plan->active,
            'amount'   => (int)$plan->amount,
            'currency' => (int)$plan->currency,
            'interval' => (string)$plan->interval,

        ];
    }
}