<?php
namespace Modules\Permission\Admin;

use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;
use SleepingOwl\Admin\Facades\TableColumn;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;

class Permissions extends Section
{

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title;

    protected $redirect = ['create' => 'display'];
    /**
     * @var string
     */
    protected $alias;

    public function getTitle()
    {
        return 'Permissions';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc'])
            ->setColumns([
                TableColumn::text('id')
                    ->setLabel('ID'),
                TableColumn::text('name')
                    ->setLabel('Name'),
                TableColumn::datetime('updated_at')
                    ->setLabel('Update At'),
                TableColumn::datetime('created_at')
                    ->setLabel('Create At'),
            ]);

        $display->setColumnFilters([
            AdminColumnFilter::text()
                ->setOperator(FilterInterface::CONTAINS)
                ->setPlaceholder('ID')
                ->setHtmlAttributes(['style' => 'width: 50px']),
            AdminColumnFilter::text()
                ->setOperator(FilterInterface::CONTAINS)
                ->setPlaceholder('Name'),
            AdminColumnFilter::range()
                ->setFrom(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                )
                ->setTo(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                ),
            AdminColumnFilter::range()
                ->setFrom(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                )
                ->setTo(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                ),
            null,
        ]);

        $display->getColumnFilters()->setPlacement('table.header');


        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return $form = AdminForm::panel()
            ->addBody(
                AdminFormElement::text('name', "Name (must content action: display, edit, create, delete. Example: permission-edit)")
                    ->required()
                    ->unique()
            );
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function onDelete($id)
    {
        return true;
    }
}
