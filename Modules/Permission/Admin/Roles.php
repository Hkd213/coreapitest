<?php
namespace Modules\Permission\Admin;

use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;
use SleepingOwl\Admin\Facades\TableColumn;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Spatie\Permission\Models\Permission;

class Roles extends Section
{

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title;
    protected $redirect = ['create' => 'display'];
    /**
     * @var string
     */
    protected $alias;

    public function getTitle()
    {
        return 'Roles';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc'])
            ->with(['permissions'])
            ->setColumns([
                TableColumn::text('id')
                    ->setLabel('ID'),
                TableColumn::link('name')
                    ->setLabel('Name'),
                TableColumn::lists('permissions.name')
                    ->setLabel('Permissions'),
                TableColumn::datetime('updated_at')
                    ->setLabel('Update At'),
                TableColumn::datetime('created_at')
                    ->setLabel('Created At'),
            ]);

        $display->setColumnFilters([
            AdminColumnFilter::text()
                ->setOperator(FilterInterface::CONTAINS)
                ->setPlaceholder('ID')
                ->setHtmlAttributes(['style' => 'width: 50px']),
            AdminColumnFilter::text()
                ->setOperator(FilterInterface::CONTAINS)
                ->setPlaceholder('Name'),
            null,
            AdminColumnFilter::range()
                ->setFrom(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                )
                ->setTo(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                ),
            AdminColumnFilter::range()
                ->setFrom(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                )
                ->setTo(
                    AdminColumnFilter::date()
                        ->setFormat(config('sleeping_owl.datetimeFormat'))
                ),
            null,
        ]);

        $display->getColumnFilters()->setPlacement('table.header');


        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return $form = AdminForm::panel()
            ->addBody(
                AdminFormElement::text('name', 'Name')
                    ->required()
                    ->unique(),
                AdminFormElement::multiselectajax('permissions', "Permissions")
                    ->setModelForOptions(new Permission())
                    ->setDisplay('name')
            );
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function onDelete($id)
    {
        return true;
    }
}
