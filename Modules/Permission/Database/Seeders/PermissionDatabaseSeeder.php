<?php

namespace Modules\Permission\Database\Seeders;

use App\Providers\AdminSectionsServiceProvider;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $roles = [
          'admin',
          'editor'
        ];

        foreach ($roles as $role) {

            Role::create(['name' => $role, 'guard_name' => 'backend']);
        }

        $permissions = [
            'display',
            'create',
            'edit',
            'delete'
        ];

        $models = app()->getProvider(AdminSectionsServiceProvider::class)->sections();

        foreach ($models as $model)
        {
            $name = basename(class_basename($model));

            foreach ($permissions as $permission) {
                Permission::create(['name' => $name.'.'.$permission, 'guard_name' => 'backend']);
            }
        }


    }
}
