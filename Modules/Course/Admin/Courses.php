<?php

namespace Modules\Course\Admin;

use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Facades\Admin;
use SleepingOwl\Admin\Facades\TableColumn;
use Modules\Section\Entities\Section;
use SleepingOwl\Admin\Section  as SectionAdmin;


class Courses extends SectionAdmin
{

    /**
     * @var string
     */
    protected $title;

    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $alias;

    public function initialize()
    {
        $this->creating(function ($config, Model $model) {
            $model->author_id = Auth::id();
            $model->type = 1;
        });
    }

    public function getTitle()
    {
        return 'Courses';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('author')
            ->setOrder([0, 'desc']);
        $display->setColumns([
            TableColumn::text('id')
                ->setLabel('ID'),
            TableColumn::text('title')
                ->setLabel('Title'),
            TableColumn::text('description', 'Description'),
            TableColumn::text('author.name', 'Author'),
            TableColumn::datetime('updated_at')
                ->setLabel('Update At'),
            TableColumn::datetime('created_at')
                ->setLabel('Create At'),
        ]);

        $display->paginate(15);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $display = AdminDisplay::tabbed();
        $display->setTabs(function () use ($id) {
            $tabs = [];
            $form = AdminForm::panel();

            $form->addBody(
                AdminFormElement::text('title', 'Title')
                    ->required(),
                AdminFormElement::ckeditor('description', 'Description')
            );

            $tabs[] = AdminDisplay::tab($form)->setLabel('Main')
                ->setActive(true)
                ->setIcon('<i class="fa fa-file-text"></i>');


            if (! is_null($id)) {
                $section = Admin::getModel(Section::class)
                    ->fireDisplay(['scopes' => ['withCourse', $id]]);
                $section->setParameter('course_id', $id);

                $tabs[] = AdminDisplay::tab($section)
                    ->setLabel('Section')
                    ->setIcon('<i class="fa fa-file-code-o"></i>');
            }

            return $tabs;
        });

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        return true;
    }
}