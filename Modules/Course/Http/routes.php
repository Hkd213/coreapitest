<?php

Route::group(['middleware' => ['auth:api','api_doc'], 'prefix' => 'v1'], function(){

    Route::get('courses/', 'Modules\Course\Http\Controllers\ApiCourseController@index');
    Route::get('courses/{id}', 'Modules\Course\Http\Controllers\ApiCourseController@show');
    Route::post('courses/', 'Modules\Course\Http\Controllers\ApiCourseController@store');

});