<?php

namespace Modules\Course\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Course\Entities\Course;
use Modules\Course\Http\Requests\CourseRequest;

/**
 * @resource Courses
 *
 * Course
 */
class ApiCourseController extends Controller
{

    /**
     * List courses
     *
     * @return mixed
     */
    public function index()
    {
        $courses = Course::with('media')->paginate(20);

        return response()->apiSuccess(['courses' => $courses]);
    }


    /**
     * Create course
     *
     * @param CourseRequest $request
     * @return mixed
     */
    public function store(CourseRequest $request)
    {
        $course = Course::create($request->all());

        return response()->apiSuccess(['course' => $course]);
    }

    /**
     * Show course
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);

        return response()->apiSuccess(['course' => $course]);
    }


}
