<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|max:255|min:7',
            'description'   => 'required|max:5000|min:7',
            'type'          => 'required|integer',
            'multi_media.*' => 'mimetypes:video/avi,video/mpeg,video/quicktime,image/jpeg,image/bmp,image/png'
        ];
    }

}