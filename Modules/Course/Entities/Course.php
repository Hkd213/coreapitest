<?php

namespace Modules\Course\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Course\Observers\CourseObserver;
use Modules\Course\Observers\LessonObserver;
use Modules\Employee\Entities\Employee;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Course extends Model implements HasMedia
{

    use SoftDeletes;
    use HasMediaTrait;

    /**
     * Load Observer
     */
    public static function boot()
    {
        self::observe(new CourseObserver());
        parent::boot();
    }

    protected $table    = 'courses';
    protected $fillable = ['title', 'description', 'author_id', 'status', 'type'];

    public function author()
    {
        return $this->hasOne(Employee::class, 'id', 'author_id');
    }

}