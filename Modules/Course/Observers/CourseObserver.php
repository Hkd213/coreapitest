<?php
namespace Modules\Course\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class CourseObserver
{
    /**
     * Model Saved Course from Eloquent
     *
     * @param $model
     */
    public function saved($model)
    {
        //array
        $multi_media = Request::file('multi_media');

        if($multi_media)
        {
            foreach ($multi_media as $media)
            {
                $model->addMedia($media)->toMediaCollection();
            }
        }
    }

    /**
     * Model Saving Course from Eloquent
     *
     * @param $model
     */
    public function saving($model)
    {
        $model->author_id = Auth::id();
        $model->type = 1;
    }
}