<?php

namespace Modules\Bookmark\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Bookmark\Entities\Bookmark;
use Modules\Bookmark\Http\Requests\BookmarkRequest;
use Illuminate\Support\Facades\Auth;

/**
 * @resource Courses
 *
 * Course
 */
class ApiBookmarkController extends Controller
{

    /**
     * List bookmark
     *
     * @return mixed
     */
    public function index()
    {
        $bookmark = Bookmark::whereRaw('1=1')->paginate(20);

        return response()->apiSuccess(['bookmark' => $bookmark]);
    }


    /**
     * Create bookmark
     *
     * @param BookmarkRequest $request
     * @return mixed
     */
    public function store(BookmarkRequest $request)
    {
        $user   = Auth::user();
        $bookmark = Bookmark::create($request->all());
        $bookmark->user_id=$user->id;
        $bookmark->save();

        return response()->apiSuccess(['bookmark' => $bookmark]);
    }

    /**
     * Edit bookmark
     *
     * @param BookmarkRequest $request
     * @return mixed
     */
    public function edit($id,BookmarkRequest $request)
    {
        $bookmark = Bookmark::find($id);
        $input             = $request->all();
        $bookmark->update($input);
        $bookmark->save();
        return response()->apiSuccess(['bookmark' => $bookmark]);
    }

    /**
     * Delete bookmark
     *
     * @param int $id
     * @return mixed
     */
    public function delete($id)
    {
        $bookmark = Bookmark::find($id);
        $bookmark->delete();
        return response()->apiSuccess([]);
    }

    /**
     * Show bookmark
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $bookmark = Bookmark::findOrFail($id);

        return response()->apiSuccess(['bookmark' => $bookmark]);
    }


}
