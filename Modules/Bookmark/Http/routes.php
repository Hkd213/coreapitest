<?php

Route::group(['middleware' => ['auth:api','api_doc'], 'prefix' => 'v1'], function(){

    Route::get('bookmark/', 'Modules\Bookmark\Http\Controllers\ApiBookmarkController@index');
    Route::get('bookmark/{id}', 'Modules\Bookmark\Http\Controllers\ApiBookmarkController@show');
    Route::post('bookmark/{id}', 'Modules\Bookmark\Http\Controllers\ApiBookmarkController@edit');
    Route::delete('bookmark/{id}', 'Modules\Bookmark\Http\Controllers\ApiBookmarkController@delete');
    Route::post('bookmark/', 'Modules\Bookmark\Http\Controllers\ApiBookmarkController@store');

});