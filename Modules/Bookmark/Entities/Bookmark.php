<?php

namespace Modules\Bookmark\Entities;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{

    protected $table    = 'bookmark';
    protected $fillable = ['name', 'content_id'];

}