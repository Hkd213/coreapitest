## Coreapi Education service
 
API DOCUMENTATION [http://85.119.150.134/docs/](http://85.119.150.134/docs)

URL BACKEND [http://85.119.150.134/backend](http://85.119.150.134/backend)
test@test.com:123123

#####Database command:

sudo -u postgres psql

#####Command install:

php artisan module:migrate
php artisan passport:install
php artisan module:seed

//for generate doc
php artisan api:generate --middleware=api_doc --noResponseCalls
