FROM php:7.1.9-fpm

WORKDIR /var/www

RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -  && \
#    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
#    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
#    apt-get update && \
#    apt-get install -y yarn && \
    apt-get install -y git && \
    apt-get install -y libmcrypt-dev \
        libpq-dev \
        libpng-dev \
        libxml2-dev \
        libxslt-dev \
        libicu-dev \
        libjpeg62-turbo-dev \
        libfreetype6-dev \
        libpng12-dev \
        libbz2-dev \
        libmagickwand-dev && \
    rm -rf /var/lib/apt/lists/*

# Imagemagick to fix
# libmagickwand-dev is version 3 at least v6 is needed and also PHP doesn't see imagemagick
# libmagickwand-dev
# pecl install imagick && docker-php-ext-enable imagick

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/  && \
        (yes | pecl install imagick mongodb) && \
        docker-php-ext-enable mongodb && \
        docker-php-ext-install \
            mcrypt \
            bcmath \
            mbstring \
            zip \
            opcache \
            pdo \
            pdo_pgsql \
            gd \
            xmlrpc \
            opcache \
            intl \
            mysqli \
            bz2 \
            json \
            exif

COPY ./docker/fpm_www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./docker/docker-php-ext-imagick.ini /usr/local/etc/php/conf.d/docker-php-ext-imagick.ini
COPY ./docker/php.ini /usr/local/etc/php/

# PHP 7.0 Compiled Modules (see commented out commans at the end of this file)
# XDebug
COPY ./docker/php_modules/xdebug.so /usr/local/lib/php/extensions/no-debug-non-zts-20151012/

# GEOS
COPY ./docker/modules/* /usr/local/lib/
RUN ln -s /usr/local/lib/libgeos-3.5.1.so /usr/local/lib/libgeos.so \
    && ln -s /usr/local/lib/libgeos_c.so.1.9.1 /usr/local/lib/libgeos_c.so \
    && ln -s /usr/local/lib/libgeos_c.so.1.9.1 /usr/local/lib/libgeos_c.so.1 \
    && ldconfig
COPY ./docker/php_modules/geos.so /usr/local/lib/php/extensions/no-debug-non-zts-20151012/
# ---------------------------------------------------------------------------------------------

#RUN git clone https://anonscm.debian.org/cgit/pkg-grass/geos.git /tmp/geos/ \
#    && cd /tmp/geos/ \
#    && ./configure \
#    && make \
#    && make install \
#    && ldconfig \


# Register the COMPOSER_HOME environment variable
ENV COMPOSER_HOME /composer
# Add global binary directory to PATH and make sure to re-export it
ENV PATH /composer/vendor/bin:$PATH
# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_VERSION 1.2.1

# Setup the Composer installer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
  && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }"

# Install Composer
RUN php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION} && rm -rf /tmp/composer-setup.php

# Install PHPUnit
RUN curl -L -o /tmp/phpunit.phar  https://phar.phpunit.de/phpunit.phar \
  && mv /tmp/phpunit.phar /usr/local/bin/phpunit \
  && chmod +x /usr/local/bin/phpunit

# Install Composer dependencies
COPY ./composer.json ./
#RUN composer install --no-scripts --no-autoloader

COPY . /var/www
RUN cp /var/www/.env.example /var/www/.env

#RUN composer update




