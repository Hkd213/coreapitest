<?php
namespace App\Providers;

use Illuminate\Support\Facades\Response;use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('apiSuccess', function ($value) {

            return response()->json(['success' => $value], 200);
        });

        Response::macro('apiError', function ($value) {

            return response()->json(['error' => $value], 500);
        });
    }
}